# List files in the directory
> ls -lart

# Initialize empty git local repository in current folder
> git init

# Create or edit file
> gedit .gitignore

# Add files to the staging
> git add .

> git --help
> git add --help

# Get list of staged files
> git status

# Configure username & email for git commits
> git config --global user.name "VipulKrSharma"
> git config --global user.email "tysonvks@gmail.com"

# Commit staged changes to local repository  with (-m) message
> git commit -m "Initial Commit"

> git status

# Attach local repository with remote repository
> git remote add origin https://VipulKumarSharma@bitbucket.org/VipulKumarSharma/git_handson.git

# Push Local commits to remote repository
> git push origin master

> git pull
